﻿using System;
using System.Collections.Generic;

namespace SpezioAnthony_CodingExercise1
{
    class Program
    {
        private static Random index = new Random();//random index to delete

        static void Main(string[] args)
        {
            /*Exercise 1 (Sorting Arrays/Lists):
            Code will alphabetize the array/list (A-Z) and backwards (Z-A).
            Code will randomly choose a name from the array/list, delete it from the array/list, then pick another one, delete it, etc, until there is nothing left in the array.*/
            List<string> brands = Populate();//list of brands
            bool loop = true; //main loop
            Console.WriteLine("Random order:");//tell user current order
            Print(brands);//print in random order
            while (loop)//while loop == true
            {
                Menu();//print menu
                string choice = Console.ReadLine().ToLower();//read input from user
                switch (choice)//compare input 
                {
                    case "1"://alphebatize a-z
                        brands.Sort();//sort a-z
                        Console.WriteLine("A-Z:");//tell user order
                        Print(brands);//print list
                        break;
                    case "2"://alphebatize z-a
                        brands.Sort((a, b) => -1 * a.CompareTo(b));//sort z-a
                        Console.WriteLine("Z-A");//tell user order
                        Print(brands);//print list
                        break;
                    case "3"://delete all records
                        for (int i = brands.Count; i > 0; i--)//for each current total 
                        {
                            brands.RemoveAt(index.Next(0, brands.Count - 1));//remove at random index
                            Console.WriteLine($"{brands.Count} left.\n");//tell user how many left
                            Print(brands);//print what is left
                        }
                        break;
                    case "4"://re-populate
                        brands = Populate();//populate list, always starts at 0
                        break;
                    case "5"://exit
                        loop = false;//stop loop
                        break;
                    default:
                        Console.WriteLine("Please enter a valid choice");//invalid input
                        break;
                }
            }



        }
        static List<string> Populate()//populate the list of brands
        {
            List<string> brands = new List<string>();//declare new string
            brands.InsertRange(0, new string[] { "Starbucks",//start at zero
                "Dunkin' Donuts",//seperate line for readability
                "Gregory's",
                "Traxx",
                "Maxwell House",
                "Nescafe",
                "Mount Hagen",
                "Douwe Egberts",
                "Folgers",
                "Jacobs",
                "Four Stigmatic",
                "Kirkland",
                "Eight O'Clock",
                "Community",
                "Trader Joe's",
                "Luzianne",
                "Kroger",
                "McCafe",
                "Cafe Bustelo",
                "Chock Full O' Nuts",
                "Gevalia",
                "Keurig",
                "Death Wish",
                "Gloria Jean's",
                "Hills Bros.",
                "Black Ivory",
                "Kraft",
                "Dunn Bros.",
                "Blue Bottle"
            });
            return brands;//return list
        }
        static void Menu()//print menu
        {
            Console.WriteLine("Main Menu:\n" +
                "1: List A - Z\n" +
                "2: List Z - A\n" +
                "3: Delete list\n" +
                "4: Re-populate\n" +
                "5: Exit\n" +
                "Enter a menu choice number: ");//ask user for choice
        }
        static void Print(List<string> brands)//print list values
        {
            int i = 0;//used to stop trailing comma at end
            foreach (string s in brands)//for each element in the list
            {
                Console.Write(s);//print value to console
                i++;//add one to get closer to end
                if (i == brands.Count)//if at the end
                {
                    Console.WriteLine("\n");//write  new line
                    break;//do not need comma
                }
                Console.Write(", ");//if not the end print comma, repeat
            }
        }
    }
}
