﻿using System;

namespace SpezioAnthony_CodingExercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Exercise 2 (Give Clues Until You Get An Answer)
            bool correct = false;//loop
            int count = 0;//how many times answered incorrectly
            while (!correct)//not correct
            {
                if (count == 0) Console.WriteLine("What is the largest selling ground coffee in the United States?");//ask 
                else if (count == 1) Console.WriteLine("The brand was founded in 1850.");//big hint
                else if (count == 2) Console.WriteLine("The Best Part of Wakin' Up is _______ in Your Cup.");//motto
                else if (count == 3) Console.WriteLine("Proctor & Gamble owned this between 1963 and 2008.");//first aquisition
                else if (count == 4) Console.WriteLine("J.M. Smuckers aquired this in 2008.");//current owner
                else if (count == 5) Console.WriteLine("William H. Bovee is the original owner.");//original founder
                else if (count == 6) Console.WriteLine("Google one of the first hints.");//tell user to look up info given
                else if (count == 7) Console.WriteLine("The answer starts with the letter 'F'.");//give letter hint
                else if (count == 8) Console.WriteLine("The answer ends with the letter 's'.");//give last letter hint
                else if (count == 9) Console.WriteLine("The second to last letter is 'r'.");//one more letter
                else if (count == 10) Console.WriteLine("F _ l _ _ r s");//place the letters with one more
                else if (count == 11) Console.WriteLine("F _ l g _ r s");//place the last constanant
                else if (count == 12) Console.WriteLine("There are only vowels missing from the previous hint");//tell user all constants are placed
                else if (count == 13) Console.WriteLine("You have answered incorrectly 13 times, no more hints will be given unless you are close");//no more hints
                string answer = Console.ReadLine();//read the line
                if (answer == "Folgers") break;//if they got it break it
                Console.WriteLine("Wrong! Try again.");//tell user they are wrong
                if (answer == "folgers") Console.WriteLine("Are you sure that is the correct case?");//tell user to capitalize
                else if (answer.ToLower() == "foldgers") Console.WriteLine("Are you positive that is the correct spelling?");//common misspelling
                else if (answer.ToLower() == "folger's") Console.WriteLine("The company has not used the apostrophe since 1963");//apostrophe in answer
                else if (answer.ToLower() == "foldger's") Console.WriteLine("There is a typo and company has not used the apostrophe since 1963");//typo and apostrophe
                else if (answer.ToLower() == "starbucks") Console.WriteLine("Starbucks has not been around that long in the ground cofee market");//statistics/history
                else if (answer.ToLower() == "maxwell house") Console.WriteLine("Good try! But no cigar.");//tell user good brand
                else Console.WriteLine("You were not close with your previous answer"); //tell user not close
                count++;//increment
            }
            Console.WriteLine("Congrats! the program will now close upon the next keypress");//congratulate user
            Console.ReadKey();
        }
    }
}
